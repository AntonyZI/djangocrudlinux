from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render
from .forms import UploadFileForm

import os
import subprocess

# Imaginary function to handle an uploaded file.
#from .handlers import handle_uploaded_file

def upload_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_file(request.FILES['file'])
            cleanFName = cleanNameDocExtension(str(request.FILES['file'])).replace(" ", "_")
            return HttpResponse("<a href='/download/" + cleanFName + ".pdf' download>Descargar</a>")
            #return HttpResponseRedirect('/success/url/')
        else:
            return HttpResponse("Formulario no valido")
    else:
        form = UploadFileForm()
    return render(request, 'upload.html', {'form': form})

tmpDir = os.path.realpath("./tmp/docToPdf")

def handle_uploaded_file(f):
    file_name = str(f).replace(" ", "_")
    with open(os.path.join(tmpDir, file_name), 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    document_to_pdf(tmpDir, file_name)

def document_to_pdf(file_path, file_name):
    print("Converting doc to pdf")
    cleanFName = cleanNameDocExtension(file_name)
    pdfPath = os.path.join(file_path, cleanFName)
    # abiword converts file formats into another, here docx to pdf
    # convert the file, using a temporary file with a random name
    command = "abiword -t %(pdfPath)s.pdf %(file_name)s && rm %(file_name)s" % locals()
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, cwd=tmpDir)
    error = p.stderr.readlines()
    if error:
        #raise Exception("".join(str(error)))
        print(str(error))
    pdfLines = p.stdout.readlines()
    print("".join(pdfLines))

def download_pdf(request, path):
    request.method == 'GET'
    if len(path) == 0:
        return HttpResponse("Ningun archivo pdf especificado")
    file_path = os.path.join(tmpDir, path)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as filePath:
            response = HttpResponse(filePath.read(), content_type="application/pdf")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    raise Http404

def cleanNameDocExtension(file_name):
    inverseName = file_name[::-1]
    fileExten = "cod." # Extension de archivo invertido
    cleanFName = inverseName[: inverseName.find(fileExten) + len(fileExten) - 1: -1]
    return cleanFName