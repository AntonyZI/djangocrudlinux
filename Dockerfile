#FROM python:3.9.16-alpine
FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

#RUN apk add --no-cache abiword
RUN apt update && apt install abiword -y && apt-get clean

COPY . .

EXPOSE 8000

ENTRYPOINT ["python", "./manage.py"]
#CMD [ "python", "./manage.py 0.0.0.0:8000" ]
CMD [ "runserver", "0.0.0.0:8000" ]
